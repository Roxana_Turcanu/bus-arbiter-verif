module busArbiter #(
parameter WIDTH  = 'd8 //numarul de biti ai datelor citite/scrise
)(

input                clk, //semnal de ceas, frecveta 50MHz
input                reset, //reset asincron, activ in 1

input                rq1_i,//semnal cerere client 1
input                wr1_ni,//tipul cererii clientului 1, 1=citire, 0=scriere
input   [WIDTH -1:0]  dataW1_i,//date scrise preluate de la clientul 1
output  reg             ack1_o,//semnal confirmare client 1
output reg [WIDTH -1:0]  dataR1_o,//date citite livrate catre clientul 1

input                rq2_i,//semnal cerere client 2
input                wr2_ni,//tipul cererii clientului 2, 1=citire, 0=scriere
input  [WIDTH -1:0]  dataW2_i,//date scrise preluate de la clientul 2
output  reg             ack2_o,//semnal confirmare client 2
output reg [WIDTH -1:0]  dataR2_o,//date citite livrate catre clientul 2

input               rq3_i,//semnal cerere client 3
input                wr3_ni,//tipul cererii clientului 3, 1=citire, 0=scriere
input   [WIDTH -1:0]  dataW3_i,//date scrise preluate de la clientul 3
output   reg           ack3_o,//semnal confirmare client 3
output reg [WIDTH -1:0]  dataR3_o,//date citite livrate catre clientul 3

input               rq4_i,//semnal cerere client 4
input               wr4_ni,//tipul cererii clientului 4, 1=citire, 0=scriere
input  [WIDTH -1:0]  dataW4_i,//date scrise preluate de la clientul 4
output  reg                ack4_o,//semnal confirmare client 4
output reg [WIDTH -1:0]  dataR4_o,//date citite livrate catre clientul 4

input                ack_i,//semnal confirmare de la server
input   [WIDTH -1:0]  dataR_i,//date citite, informatiile de la server spre client
output  reg             wr_no,//tipul cererii, 1=citire, 0=scriere
output   reg            rq_o,//semnnal cerere catre server 
output reg [WIDTH -1:0]  dataW_o//datele scrise, informatiile de la client la server

);

//registrii pentru implementarea algoritmului RR
reg ack1_oo;
reg ack2_oo;
reg ack3_oo;
reg ack4_oo;

reg [3:0] shift_req;//registru care stocheaza semnalele de request in ordinea prioritatii in functie de clientul servit anterior
reg [1:0] client_first; // registru care stocheaza informatii legate despre clientul care are cea mai mare prioritate in functie de clientul servit precedent
reg [3:0] shift_ack; // registru care stocheaza 1 pe pozitia clientului care urmeaza a fi servit, in functie de prioritate
reg [3:0] ack_final; // registru format din bitii pentru semnalele de ack pentru clienti in ordinea prioritatii 1>2>3>4
reg ack_dly;

//prioritate client1>client2>client3>client4

initial begin
wait (rq4_i | rq3_i | rq2_i | rq1_i);
   if(rq1_i) client_first <= 2'b00; else 
   if(rq2_i) client_first <= 2'b01; else 
   if(rq3_i) client_first <= 2'b10; else 
   if(rq4_i) client_first <= 2'b11; 
end

always @(posedge clk or posedge reset)
if (reset) ack_dly<=0; else 
           ack_dly<=ack_i;

always @(posedge clk or posedge reset)
if(reset) client_first <= 2'b00; else
if(ack1_oo) begin//clientul 1 a fost servit, clientul 2 este prioritar
	wait (rq4_i | rq3_i | rq2_i | rq1_i);
	if(rq2_i) client_first <= 2'b01; else
	if(rq3_i) client_first <= 2'b10; else
	if(rq4_i) client_first <= 2'b11; else
	if(rq1_i) client_first <= 2'b00;
end else
if(ack2_oo) begin//clientul 2 a fost servit, clientul 3 este prioritar
	wait (rq4_i | rq3_i | rq2_i | rq1_i);
	if(rq3_i) client_first <= 2'b10; else
	if(rq4_i) client_first <= 2'b11; else
	if(rq1_i) client_first <= 2'b00; else
	if(rq2_i) client_first <= 2'b01;
end else
if(ack3_oo) begin//clientul 3 a fost servit, clientul 4 este prioritar
	wait (rq4_i | rq3_i | rq2_i | rq1_i);
	if(rq4_i) client_first <= 2'b11; else
	if(rq1_i) client_first <= 2'b00; else
	if(rq2_i) client_first <= 2'b01; else
	if(rq3_i) client_first <= 2'b10;
end else
if(ack4_oo) begin//clientul 4 a fost servit, se revine la inceput unde clientul 1 este prioritar
	wait (rq4_i | rq3_i | rq2_i | rq1_i);
	if(rq1_i) client_first <= 2'b00; else
	if(rq2_i) client_first <= 2'b01; else
	if(rq3_i) client_first <= 2'b10; else
	if(rq4_i) client_first <= 2'b11;
end 

//in registrul shift_req pe pozitia 0 va fi requestul clientului cel mai prioritar in fiecare moment
always @(*) 
begin
case(client_first) 
   2'b00: shift_req[3:0] = {rq4_i, rq3_i, rq2_i, rq1_i};
   2'b01: shift_req[3:0] = {rq1_i, rq4_i, rq3_i, rq2_i};
   2'b10: shift_req[3:0] = {rq2_i, rq1_i, rq4_i, rq3_i};
   2'b11: shift_req[3:0] = {rq3_i, rq2_i, rq1_i, rq4_i};
endcase  
end

//shift_ack va avea 1 pe pozitia clientului care cere, ex: daca cere clientul 1,shift_ack = 0001
always @(*)
begin
   shift_ack[3:0] = 4'b0000;
   if(shift_req[0]) shift_ack[0] = 1'b1; else
   if(shift_req[1]) shift_ack[1] = 1'b1; else 
   if(shift_req[2]) shift_ack[2] = 1'b1; else
   if(shift_req[3]) shift_ack[3] = 1'b1;
end

//registru care va contine 1 pe bitul corespunzator clientului care este servit in acel moment
always @(*)
begin 
case(client_first)
   2'b00: ack_final[3:0] = shift_ack[3:0]; // clientul 1 este prioritar
   2'b01: ack_final[3:0] = {shift_ack[2:0], shift_ack[3]};
   2'b10: ack_final[3:0] = {shift_ack[1:0], shift_ack[3:2]};
   2'b11: ack_final[3:0] = {shift_ack[0], shift_ack[3:1]};
endcase

end

//generarea semnalelor de ack
always @(posedge clk or posedge reset)
if(reset) begin
   ack1_oo <= 1'b0;
   ack2_oo <= 1'b0;
   ack3_oo <= 1'b0;
   ack4_oo <= 1'b0; end 
else begin  
   if(ack_i) begin
   ack1_oo <= ack_final[0];
   ack2_oo <= ack_final[1];
   ack3_oo <= ack_final[2];
   ack4_oo <= ack_final[3];
   rq_o <= 1'b0;
   end else begin // ack trebuie sa fie activ doar o perioada de ceas 
   ack1_oo <= 1'b0;
   ack2_oo <= 1'b0;
   ack3_oo <= 1'b0;
   ack4_oo <= 1'b0;
   end

   end

always @(posedge clk or posedge reset)
if(ack_dly) rq_o <= 1'b0;

always @(posedge clk or posedge reset)
if(reset) begin
	   ack1_o = 1'b0;
      ack2_o = 1'b0;
      ack3_o = 1'b0;
      ack4_o = 1'b0;
      wr_no = 1'b0;
           
      dataR4_o = 'd0;
      dataR1_o = 'd0;
      dataR2_o = 'd0;
      dataR3_o = 'd0;
      dataW_o = 'd0;

      rq_o = 1'b0;
              
end else begin
case(ack_final)
4'b0001: begin //clientul 1 da req si este ales pentru a fi servit
           ack1_o = ack_i;
           ack2_o = 1'b0;
           ack3_o = 1'b0;
           ack4_o = 1'b0;
           wr_no = wr1_ni; 

           dataR1_o = dataR_i; 
           dataR2_o = 'd0;
           dataR3_o = 'd0;
           dataR4_o = 'd0;
           dataW_o = dataW1_i;
           rq_o = rq1_i;
   
end 
4'b0010: begin //clientul 2 da req si este ales pentru a fi servit
           ack1_o = 1'b0;
           ack2_o = ack_i;
           ack3_o = 1'b0;
           ack4_o = 1'b0;
           wr_no = wr2_ni;

           dataR2_o = dataR_i;
           dataR1_o = 'd0;
           dataR3_o = 'd0;
           dataR4_o = 'd0;
           dataW_o = dataW2_i;
           rq_o = rq2_i;
                    
end
4'b0100: begin //clientul 3 da req si este ales pentru a fi servit
           ack1_o = 1'b0;
           ack2_o = 1'b0;
           ack3_o = ack_i;
           ack4_o = 1'b0;
           wr_no = wr3_ni;
           
           dataR3_o = dataR_i;
           dataR1_o = 'd0;
           dataR2_o = 'd0;
           dataR4_o = 'd0;
           dataW_o = dataW3_i;
           rq_o = rq3_i;
                   
end 
4'b1000: begin //clientul 4 da req si este ales pentru a fi servit
           ack1_o = 1'b0;
           ack2_o = 1'b0;
           ack3_o = 1'b0;
           ack4_o = ack_i;
           wr_no = wr4_ni;
           
           dataR4_o = dataR_i;
           dataR1_o = 'd0;
           dataR2_o = 'd0;
           dataR3_o = 'd0;
           dataW_o = dataW4_i;
           rq_o = rq4_i;
                      
end
default: begin
           ack1_o = 1'b0;
           ack2_o = 1'b0;
           ack3_o = 1'b0;
           ack4_o = 1'b0;
           wr_no = 1'b0;
           
           dataR4_o = 'd0;
           dataR1_o = 'd0;
           dataR2_o = 'd0;
           dataR3_o = 'd0;
           dataW_o = 'd0;

           rq_o = 1'b0;
end
endcase end

endmodule  
