class req_ack_drv;

virtual req_ack_if vif;

// ...
function new(
  virtual req_ack_if vif_new
);
  vif = vif_new;
endfunction : new

//apelata in env
task start(int nr_trans, int delay_min, int delay_max);
  vif.req   = 0;
  vif.wr_n  = 0;
  vif.wdata = 0;
  
  @(negedge vif.rst);
  for (int i = 0 ; i < nr_trans; i++) begin
   // $display("Val lui nr_trans: %0h si val lui i: %0h la timpul %0t", nr_trans, i, $time); 
    repeat( $urandom_range(delay_min, delay_max)) @(posedge vif.clk); //asteapta random intre 0-10 tacte de nr trans ori
    drive_sig();
  end
endtask : start



task drive_sig();
 
  this.vif.req   = 'b1;   //generare de req
  this.vif.wr_n  = $urandom_range(0,1); //wr read sau write

  if(vif.wr_n == 0)  //daca wr = scriere
    this.vif.wdata = $random;  //se genereaza random date

  @(posedge vif.ack);  //se asteapta ackk
  @(posedge vif.clk);
  
  this.vif.req = 'b0;   //coboara req
  this.vif.wdata = 'h0; //resetez datele
  
  //@(posedge vif.clk);
endtask : drive_sig

endclass : req_ack_drv