`include "../ve_src/driver.sv"
`include "../ve_src/monitor.sv"
`include "../ve_src/sb.sv"
`include "../ve_src/receiver.sv"

class req_ack_env;


req_ack_drv cl0_drv;
req_ack_drv cl1_drv;
req_ack_drv cl2_drv;
req_ack_drv cl3_drv;

req_ack_monitor cl0_mon;
req_ack_monitor cl1_mon;
req_ack_monitor cl2_mon;
req_ack_monitor cl3_mon;

req_ack_receiver srv_rcv;
req_ack_monitor  srv_mon;

req_ack_sb       sb;

mailbox#(bit[31:0]) cl0_mb;
mailbox#(bit[31:0]) cl1_mb;
mailbox#(bit[31:0]) cl2_mb;
mailbox#(bit[31:0]) cl3_mb;
mailbox#(bit[31:0]) srv_mb;

int cl_trans[4];
int cl_trans_min[4];
int cl_trans_max[4];
int test_no;  // Declare test number as global inside the class

//constructorul de env 
function new(
    virtual req_ack_if i_if_cl0,
    virtual req_ack_if i_if_cl1,
    virtual req_ack_if i_if_cl2,
    virtual req_ack_if i_if_cl3,
    virtual req_ack_if i_if_srv

  );
  cl0_mb = new();
  cl1_mb = new();
  cl2_mb = new();
  cl3_mb = new();
  srv_mb = new();

 //instante pt clase
  cl0_drv = new(.vif_new(i_if_cl0));
  cl1_drv = new(.vif_new(i_if_cl1));
  cl2_drv = new(.vif_new(i_if_cl2));
  cl3_drv = new(.vif_new(i_if_cl3));
  cl0_mon = new(.vif_new(i_if_cl0),
                .send_data_mb_new(cl0_mb));
  cl1_mon = new(.vif_new(i_if_cl1),
                .send_data_mb_new(cl1_mb));
  cl2_mon = new(.vif_new(i_if_cl2),
                .send_data_mb_new(cl2_mb));
  cl3_mon = new(.vif_new(i_if_cl3),
                .send_data_mb_new(cl3_mb));
  srv_rcv = new(.vif_new(i_if_srv));

  srv_mon = new(.vif_new(i_if_srv),
                .send_data_mb_new(srv_mb));
  

  sb = new(.cl0_mb_new(cl0_mb), .cl1_mb_new(cl1_mb),
           .cl2_mb_new(cl2_mb), .cl3_mb_new(cl3_mb), .srv_mb_new(srv_mb), 
           .cl0_vif(i_if_cl0), .cl1_vif(i_if_cl1), .cl2_vif(i_if_cl2), .cl3_vif(i_if_cl3),.srv_vif(i_if_srv));
                
  void'($value$plusargs("TEST_NO=%0d", test_no));
  begin
    $display("test number = %0d", test_no);
  end
endfunction : new

//function void file_read(string filename);
function void file_read();
int file;
int result;
int i;

//file = $fopen(filename,"r");
file = $fopen($sformatf("test_%0d.txt",test_no),"r"); //Open the file in and get the values from the file
for(i=0; i<4; i++) begin
  result = $fscanf(file, "%0d %0d %0d", cl_trans[i], cl_trans_min[i], cl_trans_max[i]);
  //$display("cl trans din env: %0d", cl_trans[i]);
end
$fclose(file);
endfunction: file_read

function void file_write();
  int file;

  file = $fopen("test_status.txt", "a");
  $fwrite(file, "Test complete!");
  $fclose(file);
endfunction

task start();
  fork
    // TODO : start the rest of the passive objects here (ex. receiver, monitor, sb) = done
    srv_rcv.start();
    srv_mon.start();
    cl0_mon.start();
    cl1_mon.start();
    cl2_mon.start();
    cl3_mon.start();
    sb.start();

  join_none

  //file_read("test_0.txt");
  file_read();

  fork
    // TODO : start the rest of the active objects here (ex. drivers) = done
    //nr of tranzactions
    cl0_drv.start(cl_trans[0], cl_trans_min[0], cl_trans_max[0]);
    cl1_drv.start(cl_trans[1], cl_trans_min[1], cl_trans_max[1]); 
    cl2_drv.start(cl_trans[2], cl_trans_min[2], cl_trans_max[2]);
    cl3_drv.start(cl_trans[3], cl_trans_min[3], cl_trans_max[3]);
    
  join
  file_write();
endtask : start


endclass : req_ack_env