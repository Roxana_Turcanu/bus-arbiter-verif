class req_ack_monitor;

virtual req_ack_if vif;

//se trimit date de la monitor catre sb
mailbox#(bit[31:0]) send_data_mb;

//contructor
function new(
  virtual req_ack_if vif_new,
  mailbox#(bit[31:0]) send_data_mb_new
);
  vif = vif_new;
  send_data_mb = send_data_mb_new;
endfunction : new

task start();
  @(negedge vif.rst);
  forever begin
    monitor_sig();
  end
endtask : start

task monitor_sig();

  int data;
  @(posedge vif.ack); 
  if(!vif.wr_n)           //daca wr = scriere
    data = vif.wdata;     //in data se vor stoca datele de scriere
  else
    data = vif.rdata;     //in data se vor stoca datele de citire
    
  send_data_mb.put(data); //se pun datele in data prin mailbox
  //$display("Size mb: %0h, Datele din monitor%m:%0h, timp %0t",send_data_mb.num(), data, $time);
endtask : monitor_sig


endclass : req_ack_monitor