class req_ack_receiver;

virtual req_ack_if vif;

function new(
  virtual req_ack_if vif_new
);
  vif = vif_new;
endfunction : new

task start();
  //@(posedge vif.rst)
  vif.rdata <= 0;
  vif.ack <= 0;
  @(negedge vif.rst);
  forever begin
    drive_sig();
  end
endtask : start


task drive_sig();
  //vif.ack <= 'b0;
  //vif.rdata <= 'b0;

  wait(vif.req);
  //@(posedge vif.req);  //asteapta req
  
  repeat ($urandom_range(1,10)) @(posedge vif.clk); //delay intre 1-10 tacte
  if(vif.wr_n)        //daca wr = citire
    this.vif.rdata = $random;  //generare date random
  this.vif.ack = 'b1;          //se ridica ack
  @(posedge vif.clk);   
  this.vif.ack = 'b0;         //dupa in tact ack = 0
  this.vif.rdata = 'h0;       //resetare date
  @(posedge vif.clk); 
endtask : drive_sig

endclass : req_ack_receiver