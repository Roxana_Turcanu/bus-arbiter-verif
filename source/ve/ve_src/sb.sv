class req_ack_sb;

  mailbox#(bit[31:0]) cl0_mb; //declarare mailbox
  mailbox#(bit[31:0]) cl1_mb;
  mailbox#(bit[31:0]) cl2_mb;
  mailbox#(bit[31:0]) cl3_mb;
  mailbox#(bit[31:0]) srv_mb;

  virtual req_ack_if cl0_vif;  //declarare interfate
  virtual req_ack_if cl1_vif;
  virtual req_ack_if cl2_vif;
  virtual req_ack_if cl3_vif;
  virtual req_ack_if srv_vif;

  int current_client;
  int arb_array[4];
  int read_index;
  int write_index;

  string temp_string;

  function new(                     //constructor
    mailbox#(bit[31:0]) cl0_mb_new, //parametrii constructorului
    mailbox#(bit[31:0]) cl1_mb_new,
    mailbox#(bit[31:0]) cl2_mb_new,
    mailbox#(bit[31:0]) cl3_mb_new,
    mailbox#(bit[31:0]) srv_mb_new,
    virtual req_ack_if cl0_vif,
    virtual req_ack_if cl1_vif,
    virtual req_ack_if cl2_vif,
    virtual req_ack_if cl3_vif,
    virtual req_ack_if srv_vif
   
  );
    cl0_mb = cl0_mb_new;
    cl1_mb = cl1_mb_new;
    cl2_mb = cl2_mb_new;
    cl3_mb = cl3_mb_new;
    srv_mb = srv_mb_new;
 
    this.cl0_vif = cl0_vif;
    this.cl1_vif = cl1_vif;
    this.cl2_vif = cl2_vif;
    this.cl3_vif = cl3_vif;
    this.srv_vif = srv_vif;

    current_client = 5;
    read_index = 0;
    write_index = 0;
  endfunction : new


  function void file_write(string message);
  int file;

  file = $fopen("test_status.txt", "a");
  $fwrite(file, message);
  $fclose(file);
  endfunction

  task start();
    bit[31:0] cl0_data;
    bit[31:0] cl1_data;
    bit[31:0] cl2_data;
    bit[31:0] cl3_data;
    bit[31:0] srv_data; 

    wait(cl0_vif.req || cl1_vif.req || cl2_vif.req || cl3_vif.req);
    if(cl0_vif.req) current_client = 0; else
    if(cl1_vif.req) current_client = 1; else
    if(cl2_vif.req) current_client = 2; else
    if(cl3_vif.req) current_client = 3; 

    forever begin
    @(posedge(cl0_vif.ack || cl1_vif.ack || cl2_vif.ack || cl3_vif.ack)); //se verifica daca exista ack
    #10;
    begin 
    if(cl0_vif.ack && current_client != 0) begin
      $display("Cl0 Received ack instead of %0d at time: %0t", current_client, $time);
      file_write("ERROR: Cl0 received ack! \n");
      $stop;
    end
    else if(cl1_vif.ack && current_client != 1) begin
      $display("Cl1 Received ack instead of %0d at time %0t", current_client, $time);
      file_write("ERROR: Cl1 received ack! \n");
      $stop;
    end
    else if(cl2_vif.ack && current_client != 2) begin
      $display("Cl2 Received ack instead of %0d at time %0t", current_client, $time);
      temp_string = {"Cl2 Received ack instead of %0d ", string'(current_client)};
      file_write("ERROR: Cl2 received ack! \n");
      $stop;
    end
    else if(cl3_vif.ack && current_client != 3) begin
      $display("Cl3 Received ack instead of %0d at time %0t", current_client, $time);
      temp_string = {"Cl3 Received ack instead of %0d ", string'(current_client)};
      file_write("ERROR: Cl3 received ack! \n");
      $stop;
    end 
    
    //num -> Returns the number of messages currently in the mailbox
    if(current_client == 0) begin
      if(cl0_mb.num() > 0 ) begin //verific daca exista date in mailbox
        srv_mb.get(srv_data);
        cl0_mb.get(cl0_data);
        if(srv_data ==  cl0_data) begin //verific daca datle din server = datele din client
          $display("Cl_0: Data match, data from client: %0h data from server: %0h at time: %0t", cl0_data, srv_data, $time);
        end else begin
          $display("Cl_0: Data mismatch, data from client: %0h data from server: %0h at time: %0t", cl0_data, srv_data, $time);
          file_write("Cl_0: Data mismatch \n");
          $stop;
        end
      end else begin #10;
        $display("Got data: %0h from mailbox, client 0 wasn't served as expected; timp: %0t",cl0_data,$time);
        $stop; 
      end 
    if(cl1_vif.req) current_client = 1; else
    if(cl2_vif.req) current_client = 2; else
    if(cl3_vif.req) current_client = 3;
    end 

    else if(current_client == 1) begin
      if(cl1_mb.num() > 0 ) begin //verific daca exista date in mailbox
        srv_mb.get(srv_data);
        cl1_mb.get(cl1_data);
        if(srv_data ==  cl1_data) begin//verific daca datle din server = datele din client
          $display("Cl_1: Data match, data from client: %0h data from server: %0h at time: %0t", cl1_data, srv_data, $time);
        end else begin
          $display("Cl_1: Data mismatch, data from client: %0h data from server: %0h at time: %0t", cl1_data, srv_data, $time);
          file_write("Cl_1: Data mismatch \n");
          $stop;
        end
      end else begin #10;
        $display("Got data: %0h from mailbox, client 1 wasn't served as expected; timp: %0t",cl1_data,$time);
      end
    if(cl2_vif.req) current_client = 2; else
    if(cl3_vif.req) current_client = 3; else
    if(cl0_vif.req) current_client = 0;
    end

    else if(current_client == 2) begin
      if(cl2_mb.num() > 0 ) begin //verific daca exista date in mailbox
        srv_mb.get(srv_data);
        $display("Datele din srv: %0h, timpul:%0t", srv_data, $time);
        cl2_mb.get(cl2_data);
        if(srv_data ==  cl2_data) begin//verific daca datle din server = datele din client
          $display("Cl_2: Data match, data from client: %0h data from server: %0h at time: %0t", cl2_data, srv_data, $time);
        end else begin
          $display("Cl_2: Data mismatch, data from client: %0h data from server: %0h at time: %0t", cl2_data, srv_data, $time);
          file_write("Cl_2: Data mismatch \n");
          $stop;
        end
      end else begin #10;
        $display("Got data: %0h from mailbox, client 2 wasn't served as expected; timp: %0t",cl2_data,$time);
      end
    if(cl3_vif.req) current_client = 3; else
    if(cl0_vif.req) current_client = 0; else
    if(cl1_vif.req) current_client = 1;
    end

    else if(current_client == 3) begin
      if(cl3_mb.num() > 0 ) begin//verific daca exista date in mailbox
        srv_mb.get(srv_data);
        cl3_mb.get(cl3_data);
        if(srv_data ==  cl3_data) begin//verific daca datle din server = datele din client
          $display("Cl_3: Data match, data from client: %0h data from server: %0h at time: %0t", cl3_data, srv_data, $time);
        end else begin
          $display("Cl_3: Data mismatch, data from client: %0h data from server: %0h at time: %0t", cl3_data, srv_data, $time);
          file_write("Cl_3: Data mismatch \n");
          $stop;
        end
      end else begin #10;
        $display("Got data: %0h from mailbox, client 3 wasn't served as expected; timp: %0t",cl3_data, $time);
      end
    if(cl0_vif.req) current_client = 0; else
    if(cl1_vif.req) current_client = 1; else
    if(cl2_vif.req) current_client = 2;
    end

  end
  end
  endtask : start



endclass : req_ack_sb