//`include"env.sv"
program req_ack_tb(
    req_ack_if i_if_cl0,
    req_ack_if i_if_cl1,
    req_ack_if i_if_cl2,
    req_ack_if i_if_cl3,
    req_ack_if i_if_srv
);

//definire env
req_ack_env env;

initial
begin
//initializare env
env = new(.i_if_cl0(i_if_cl0),
          .i_if_cl1(i_if_cl1), 
          .i_if_cl2(i_if_cl2), 
          .i_if_cl3(i_if_cl3),
          .i_if_srv(i_if_srv)
);

env.start();

end

endprogram : req_ack_tb