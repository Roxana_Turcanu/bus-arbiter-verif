//`include"env.sv"
module req_ack_top;

bit clk;    //generare clk
always #5 clk = ~clk;
bit rst;  
initial begin   //generare reset
  rst = 1;
  #10;
  rst = 0;
end
//inferfete pt cl si srv
req_ack_if i_if_cl0 (.clk(clk), .rst(rst));
req_ack_if i_if_cl1 (.clk(clk), .rst(rst));
req_ack_if i_if_cl2 (.clk(clk), .rst(rst));
req_ack_if i_if_cl3 (.clk(clk), .rst(rst));
req_ack_if i_if_srv (.clk(clk), .rst(rst));


req_ack_tb tb(
  .i_if_cl0(i_if_cl0),
  .i_if_cl1(i_if_cl1),
  .i_if_cl2(i_if_cl2),
  .i_if_cl3(i_if_cl3),
  .i_if_srv(i_if_srv)
);

// TODO : Change the below instance with your RTL = done
bus_arbiter i_req_ack 
(
  .clk            (clk),
  .reset          (rst),
  .rq1_i          (i_if_cl0.req),
  .rq2_i          (i_if_cl1.req),
  .rq3_i          (i_if_cl2.req),
  .rq4_i          (i_if_cl3.req),
  
  .ack1_o        (i_if_cl0.ack),
  .ack2_o        (i_if_cl1.ack),
  .ack3_o        (i_if_cl2.ack),
  .ack4_o        (i_if_cl3.ack),
  
  .wr1_ni        (i_if_cl0.wr_n),
  .wr2_ni        (i_if_cl1.wr_n),
  .wr3_ni        (i_if_cl2.wr_n),
  .wr4_ni        (i_if_cl3.wr_n),
  
  .dataR1_o      (i_if_cl0.rdata),
  .dataR2_o      (i_if_cl1.rdata),
  .dataR3_o      (i_if_cl2.rdata),
  .dataR4_o      (i_if_cl3.rdata),
  
  .dataW1_i      (i_if_cl0.wdata),
  .dataW2_i      (i_if_cl1.wdata),
  .dataW3_i      (i_if_cl2.wdata),
  .dataW4_i      (i_if_cl3.wdata),

  .dataW_o       (i_if_srv.wdata),
  .rq_o          (i_if_srv.req),
  .wr_no         (i_if_srv.wr_n),
  .dataR_i       (i_if_srv.rdata),
  .ack_i         (i_if_srv.ack)
);


//two clients requesting at the same time
property p_req2_comb;
  @(posedge clk) disable iff(rst)
  ((i_if_cl0.req && i_if_cl1.req && !i_if_cl2.req && !i_if_cl3.req)||
  (i_if_cl0.req && !i_if_cl1.req && i_if_cl2.req && !i_if_cl3.req) ||
  (i_if_cl0.req && !i_if_cl1.req && !i_if_cl2.req && i_if_cl3.req) ||
  (!i_if_cl0.req && i_if_cl1.req && i_if_cl2.req && !i_if_cl3.req) ||
  (!i_if_cl0.req && i_if_cl1.req && i_if_cl2.req && i_if_cl3.req)  ||
  (!i_if_cl0.req && !i_if_cl1.req && i_if_cl2.req && i_if_cl3.req) );
endproperty;

//three clients requesting at the same time
property p_req3_comb;
  @(posedge clk) disable iff(rst)
  ((i_if_cl0.req && i_if_cl1.req && i_if_cl2.req && !i_if_cl3.req)||
  (i_if_cl0.req  && !i_if_cl1.req&& i_if_cl2.req && i_if_cl3.req) ||
  (i_if_cl0.req  && i_if_cl1.req && !i_if_cl2.req&& i_if_cl3.req) ||
  (!i_if_cl0.req && i_if_cl1.req && i_if_cl2.req && i_if_cl3.req));
endproperty;

//all clients are requesting at the same time
property p_req_all_comb;
  @(posedge clk) disable iff(rst)
  ((i_if_cl0.req && i_if_cl1.req && i_if_cl2.req && i_if_cl3.req));
endproperty;

//clients requesting one at a time
property p_req_comb;
  @(posedge clk) disable iff(rst)
  ((i_if_cl0.req && !i_if_cl1.req && !i_if_cl2.req && !i_if_cl3.req)|
  (!i_if_cl0.req && i_if_cl1.req  && !i_if_cl2.req && !i_if_cl3.req) |
  (!i_if_cl0.req && !i_if_cl1.req && i_if_cl2.req  && !i_if_cl3.req) |
  (!i_if_cl0.req && !i_if_cl1.req && !i_if_cl2.req && i_if_cl3.req));
endproperty;

cp_req2_comb    :cover property(p_req2_comb);
cp_req3_comb    :cover property(p_req3_comb);
cp_req_comb     :cover property(p_req_comb);
cp_req_all_comb :cover property(p_req_all_comb);

endmodule : req_ack_top
